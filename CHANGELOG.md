## 0.0.1

* Initial version

## 0.0.2

* Rearrange the code to render and print according to different platforms

## 0.0.3

* Add descriptions, pictures, notes

## 0.0.4

* Change description image path

## 0.0.5

* Adjust the scoring item

## 0.0.6

* Change description image path

## 0.0.7

* Change the tag parameter position

## 0.0.8

* Corrected README