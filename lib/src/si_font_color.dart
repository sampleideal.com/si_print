/// this is set font color
enum SIFontColor {
  black(colorCode: "\x1B[30;1m"),
  red(colorCode: "\x1B[31;1m"),
  green(colorCode: "\x1B[32;1m"),
  yellow(colorCode: "\x1B[33;1m"),
  blue(colorCode: "\x1B[34;1m"),
  magenta(colorCode: "\x1B[35;1m"),
  cyan(colorCode: "\x1B[36;1m"),
  white(colorCode: "\x1B[37;1m"),
  theEnd(colorCode: "\x1B[0m"),
  none(colorCode: "");

  final String colorCode;

  const SIFontColor({required this.colorCode});
}

/// this is set background color
enum SIFontBgColor {
  black(colorCode: "\x1B[40;1m"),
  red(colorCode: "\x1B[41;1m"),
  green(colorCode: "\x1B[42;1m"),
  yellow(colorCode: "\x1B[43;1m"),
  blue(colorCode: "\x1B[44;1m"),
  magenta(colorCode: "\x1B[45;1m"),
  cyan(colorCode: "\x1B[46;1m"),
  white(colorCode: "\x1B[47;1m"),
  theEnd(colorCode: "\x1B[0m"),
  none(colorCode: "");

  final String colorCode;

  const SIFontBgColor({required this.colorCode});
}
