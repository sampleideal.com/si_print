import 'dart:io';

import 'si_font_color.dart';

export 'si_print.dart';

String getFontColor(SIFontColor siFontColor) {
  switch (siFontColor) {
    case SIFontColor.black:
      return Platform.isIOS ? "⚫⚫⚫⚫⚫" : "\x1B[30;1m";
    case SIFontColor.red:
      return Platform.isIOS ? "🔴🔴🔴🔴🔴" : "\x1B[31;1m";
    case SIFontColor.green:
      return Platform.isIOS ? "🟢🟢🟢🟢🟢" : "\x1B[32;1m";
    case SIFontColor.yellow:
      return Platform.isIOS ? "🟡🟡🟡🟡🟡" : "\x1B[33;1m";
    case SIFontColor.blue:
      return Platform.isIOS ? "🔵🔵🔵🔵🔵" : "\x1B[34;1m";
    case SIFontColor.magenta:
      return Platform.isIOS ? "🟠🟠🟠🟠🟠" : "\x1B[35;1m";
    case SIFontColor.cyan:
      return Platform.isIOS ? "🟣🟣🟣🟣🟣" : "\x1B[36;1m";
    case SIFontColor.white:
      return Platform.isIOS ? "⚪⚪⚪⚪⚪" : "\x1B[37;1m";
    case SIFontColor.theEnd:
      return Platform.isIOS ? "✨🏁🔚🏁✨" : "\x1B[0m";
    default:
      return "";
  }
}

String getFontBgColor(SIFontBgColor siFontColor) {
  switch (siFontColor) {
    case SIFontBgColor.black:
      return Platform.isIOS ? "⬛⬛⬛⬛⬛" : "\x1B[40;1m";
    case SIFontBgColor.red:
      return Platform.isIOS ? "🟥🟥🟥🟥🟥" : "\x1B[41;1m";
    case SIFontBgColor.green:
      return Platform.isIOS ? "🟩🟩🟩🟩🟩" : "\x1B[42;1m";
    case SIFontBgColor.yellow:
      return Platform.isIOS ? "🟨🟨🟨🟨🟨" : "\x1B[43;1m";
    case SIFontBgColor.blue:
      return Platform.isIOS ? "🟦🟦🟦🟦🟦" : "\x1B[44;1m";
    case SIFontBgColor.magenta:
      return Platform.isIOS ? "🟧🟧🟧🟧🟧" : "\x1B[45;1m";
    case SIFontBgColor.cyan:
      return Platform.isIOS ? "🟪🟪🟪🟪🟪" : "\x1B[46;1m";
    case SIFontBgColor.white:
      return Platform.isIOS ? "⬜⬜⬜⬜⬜" : "\x1B[47;1m";
    case SIFontBgColor.theEnd:
      return Platform.isIOS ? "✨🏁🔚🏁✨" : "\x1B[0m";
    default:
      return "";
  }
}
