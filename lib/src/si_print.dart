import 'package:flutter/material.dart';

import 'si_font_color.dart';
import 'si_print_func.dart' if (dart.library.html) 'si_print_func_web.dart';
import 'si_user_emoji.dart';

/// this is siPrint
class SIPrint {
  /// username
  String user;

  /// if enable
  bool enable;

  /// user Emoji
  SIUserEmoji siUserEmoji;

  /// font color
  SIFontColor siFontColor;

  /// font background color
  SIFontBgColor? siFontBgColor;

  SIPrint({
    this.user = "SIPrint",
    this.enable = true,
    this.siUserEmoji = SIUserEmoji.user,
    this.siFontColor = SIFontColor.white,
    this.siFontBgColor,
  });

  /// show print text
  void showPrint(String string, {String tag = ""}) {
    if (enable && string != "") {
      _strPrint(
        tag: tag,
        str: string,
        user: user,
        emoji: siUserEmoji.icon,
        bgColor: siFontBgColor,
        fontColor: siFontColor,
      );
    }
  }

  static void _strPrint({
    required String tag,
    required String str,
    required String user,
    required String emoji,
    SIFontBgColor? bgColor,
    required SIFontColor fontColor,
  }) {
    String bgColorStr = bgColor != null ? getFontBgColor(bgColor) : "";
    String fontColorStr = getFontColor(fontColor);
    String showStr = _user(emoji, user, tag);
    String endColorStr = getFontColor(SIFontColor.theEnd);
    debugPrint(
      "$bgColorStr$fontColorStr$showStr $str ${fontColorStr != "" || bgColorStr != "" ? endColorStr : ""}",
    );
  }

  static String _user(String emoji, String user, String tag) {
    var retTag = tag == "" ? "" : "-$tag";
    return "[$emoji:$user$retTag]";
  }

  /// font color black
  static void black(
    String string, {
    String user = "Sample",
    String tag = "",
    SIUserEmoji emoji = SIUserEmoji.user,
  }) {
    _strPrint(
      tag: tag,
      str: string,
      user: user,
      emoji: emoji.icon,
      fontColor: SIFontColor.black,
    );
  }

  /// font color red
  static void red(
    String string, {
    String user = "Sample",
    String tag = "",
    SIUserEmoji emoji = SIUserEmoji.user,
  }) {
    _strPrint(
      tag: tag,
      str: string,
      user: user,
      emoji: emoji.icon,
      fontColor: SIFontColor.red,
    );
  }

  /// font color green
  static void green(
    String string, {
    String user = "Sample",
    String tag = "",
    SIUserEmoji emoji = SIUserEmoji.user,
  }) {
    _strPrint(
      tag: tag,
      str: string,
      user: user,
      emoji: emoji.icon,
      fontColor: SIFontColor.green,
    );
  }

  /// font color yellow
  static void yellow(
    String string, {
    String user = "Sample",
    String tag = "",
    SIUserEmoji emoji = SIUserEmoji.user,
  }) {
    _strPrint(
      tag: tag,
      str: string,
      user: user,
      emoji: emoji.icon,
      fontColor: SIFontColor.yellow,
    );
  }

  /// font color blue
  static void blue(
    String string, {
    String user = "Sample",
    String tag = "",
    SIUserEmoji emoji = SIUserEmoji.user,
  }) {
    _strPrint(
      tag: tag,
      str: string,
      user: user,
      emoji: emoji.icon,
      fontColor: SIFontColor.blue,
    );
  }

  /// font color magenta
  static void magenta(
    String string, {
    String user = "Sample",
    String tag = "",
    SIUserEmoji emoji = SIUserEmoji.user,
  }) {
    _strPrint(
      tag: tag,
      str: string,
      user: user,
      emoji: emoji.icon,
      fontColor: SIFontColor.magenta,
    );
  }

  /// font color cyan
  static void cyan(
    String string, {
    String user = "Sample",
    String tag = "",
    SIUserEmoji emoji = SIUserEmoji.user,
  }) {
    _strPrint(
      tag: tag,
      str: string,
      user: user,
      emoji: emoji.icon,
      fontColor: SIFontColor.cyan,
    );
  }

  /// font color white
  static void white(
    String string, {
    String user = "Sample",
    String tag = "",
    SIUserEmoji emoji = SIUserEmoji.user,
  }) {
    _strPrint(
      tag: tag,
      str: string,
      user: user,
      emoji: emoji.icon,
      fontColor: SIFontColor.white,
    );
  }

  /// background color black
  static void blackBg(
    String string, {
    String user = "Sample",
    String tag = "",
    SIUserEmoji emoji = SIUserEmoji.user,
  }) {
    _strPrint(
      tag: tag,
      str: string,
      user: user,
      emoji: emoji.icon,
      fontColor: SIFontColor.none,
      bgColor: SIFontBgColor.black,
    );
  }

  /// background color red
  static void redBg(
    String string, {
    String user = "Sample",
    String tag = "",
    SIUserEmoji emoji = SIUserEmoji.user,
  }) {
    _strPrint(
      tag: tag,
      str: string,
      user: user,
      emoji: emoji.icon,
      fontColor: SIFontColor.none,
      bgColor: SIFontBgColor.red,
    );
  }

  /// background color green
  static void greenBg(
    String string, {
    String user = "Sample",
    String tag = "",
    SIUserEmoji emoji = SIUserEmoji.user,
  }) {
    _strPrint(
      tag: tag,
      str: string,
      user: user,
      emoji: emoji.icon,
      fontColor: SIFontColor.none,
      bgColor: SIFontBgColor.green,
    );
  }

  /// background color yellow
  static void yellowBg(
    String string, {
    String user = "Sample",
    String tag = "",
    SIUserEmoji emoji = SIUserEmoji.user,
  }) {
    _strPrint(
      tag: tag,
      str: string,
      user: user,
      emoji: emoji.icon,
      fontColor: SIFontColor.none,
      bgColor: SIFontBgColor.yellow,
    );
  }

  /// background color blue
  static void blueBg(
    String string, {
    String user = "Sample",
    String tag = "",
    SIUserEmoji emoji = SIUserEmoji.user,
  }) {
    _strPrint(
      tag: tag,
      str: string,
      user: user,
      emoji: emoji.icon,
      fontColor: SIFontColor.none,
      bgColor: SIFontBgColor.blue,
    );
  }

  /// background color magenta
  static void magentaBg(
    String string, {
    String user = "Sample",
    String tag = "",
    SIUserEmoji emoji = SIUserEmoji.user,
  }) {
    _strPrint(
      tag: tag,
      str: string,
      user: user,
      emoji: emoji.icon,
      fontColor: SIFontColor.none,
      bgColor: SIFontBgColor.magenta,
    );
  }

  /// background color cyan
  static void cyanBg(
    String string, {
    String user = "Sample",
    String tag = "",
    SIUserEmoji emoji = SIUserEmoji.user,
  }) {
    _strPrint(
      tag: tag,
      str: string,
      user: user,
      emoji: emoji.icon,
      fontColor: SIFontColor.none,
      bgColor: SIFontBgColor.cyan,
    );
  }

  /// background color white
  static void whiteBg(
    String string, {
    String user = "Sample",
    String tag = "",
    SIUserEmoji emoji = SIUserEmoji.user,
  }) {
    _strPrint(
      tag: tag,
      str: string,
      user: user,
      emoji: emoji.icon,
      fontColor: SIFontColor.none,
      bgColor: SIFontBgColor.white,
    );
  }
}
