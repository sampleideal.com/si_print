/// this is set head emoji
enum SIUserEmoji {
  user(icon: "👤"),
  mouse(icon: "🐭"),
  rabbit(icon: "🐰"),
  dragon(icon: "🐲"),
  pig(icon: "🐷"),
  tiger(icon: "🐯"),
  koala(icon: "🐨"),
  panda(icon: "🐼"),
  monkey(icon: "🐵"),
  lion(icon: "🦁"),
  poop(icon: "💩");

  final String icon;

  const SIUserEmoji({required this.icon});
}
