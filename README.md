# SIPrint
This is a class package for Flutter print.
## Display
| iOS | Android | web | MacOS |
|:------------:|:------------:|:------------:|:------------:|
| ![](https://gitlab.com/sampleideal.com/si_print/-/raw/main/example/assets/iosImage.png) | ![](https://gitlab.com/sampleideal.com/si_print/-/raw/main/example/assets/androidImage.png)| ![](https://gitlab.com/sampleideal.com/si_print/-/raw/main/example/assets/webImage.png) |![](https://gitlab.com/sampleideal.com/si_print/-/raw/main/example/assets/macOSImage.png) |
## Getting Started
### use class
```
final SIPrint samplePrint = SIPrint(
	user: "Sample",
	siUserEmoji: SIUserEmoji.user,
	siFontColor: SIFontColor.yellow,
	siFontBgColor: SIFontBgColor.white,
);
```
```
samplePrint.showPrint("Demo SIPrint");
samplePrint.showPrint("Demo SIPrint", tag: "api");
```
### use directly
```
SIPrint.black("Demo SIPrint", user: "Hope");
SIPrint.red("Demo SIPrint", user: "Hope", tag: "Cat");
SIPrint.green("Demo SIPrint", user: "Hope", emoji: SIUserEmoji.dragon);
SIPrint.yellow(
  "Demo SIPrint",
  user: "Hope",
  tag: "Cat",
  emoji: SIUserEmoji.dragon,
);
SIPrint.blue("Demo SIPrint", tag: "Pig");
SIPrint.magenta("Demo SIPrint", tag: "Pig", emoji: SIUserEmoji.pig);
SIPrint.cyan("Demo SIPrint");
SIPrint.white("Demo SIPrint");
SIPrint.blackBg("Demo SIPrint");
SIPrint.redBg("Demo SIPrint");
SIPrint.greenBg("Demo SIPrint");
SIPrint.yellowBg("Demo SIPrint");
SIPrint.blueBg("Demo SIPrint");
SIPrint.magentaBg("Demo SIPrint");
SIPrint.cyanBg("Demo SIPrint");
SIPrint.whiteBg("Demo SIPrint");
```
### Property description
| parameter name | type | description |
| ------------ | ------------ | ------------ |
| user | String | user name |
| tag | String | tag name |
| enable | bool | set enable or disable |
| siUserEmoji | SIUserEmoji | head emoji |
| siFontColor | SIFontColor | font color |
| siFontBgColor | SIFontBgColor? | font background color |
### emoji list
| Parameters | Categories |
| ------------ | ------------ |
| user | 👤 |
| mouse | 🐭 |
| rabbit | 🐰 |
| dragon | 🐲 |
| pig | 🐷 |
| tiger | 🐯 |
| koala | 🐨 |
| panda | 🐼 |
| monkey | 🐵 |
| lion | 🦁 |
| poop | 💩 |