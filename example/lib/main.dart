import 'package:flutter/material.dart';
import 'package:si_print/si_print.dart';

final SIPrint siPrint = SIPrint(
  user: "Sample",
  siUserEmoji: SIUserEmoji.user,
  siFontColor: SIFontColor.yellow,
  siFontBgColor: SIFontBgColor.white,
);

void main() {
  siPrint.showPrint("Demo SIPrint");
  siPrint.showPrint("Demo SIPrint", tag: "api");
  SIPrint.black("Demo SIPrint", user: "Hope");
  SIPrint.red("Demo SIPrint", user: "Hope", tag: "Cat");
  SIPrint.green("Demo SIPrint", user: "Hope", emoji: SIUserEmoji.dragon);
  SIPrint.yellow(
    "Demo SIPrint",
    user: "Hope",
    tag: "Cat",
    emoji: SIUserEmoji.dragon,
  );
  SIPrint.blue("Demo SIPrint", tag: "Pig");
  SIPrint.magenta("Demo SIPrint", tag: "Pig", emoji: SIUserEmoji.pig);
  SIPrint.cyan("Demo SIPrint");
  SIPrint.white("Demo SIPrint");
  SIPrint.blackBg("Demo SIPrint");
  SIPrint.redBg("Demo SIPrint");
  SIPrint.greenBg("Demo SIPrint");
  SIPrint.yellowBg("Demo SIPrint");
  SIPrint.blueBg("Demo SIPrint");
  SIPrint.magentaBg("Demo SIPrint");
  SIPrint.cyanBg("Demo SIPrint");
  SIPrint.whiteBg("Demo SIPrint");
  runApp(const HomeView());
}

class HomeView extends StatelessWidget {
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(title: const Text("Demo Print")),
        body: const Center(child: Text("Demo Print")),
      ),
    );
  }
}
